


// 1


fetch('https://jsonplaceholder.typicode.com/todos')
  .then((response) => response.json())
  .then((json) => console.log(json));





// 2

 
fetch('https://jsonplaceholder.typicode.com/todos').then(response => response.json()).then(data => {

   	let titleValue =  data.map((elem) => {

   		 return (elem.title)

      })

   		console.log(titleValue);
});
   




// 3



 fetch('https://jsonplaceholder.typicode.com/todos/1')
  .then((response) => response.json())
  .then((json) => console.log(json));




// 4



fetch('https://jsonplaceholder.typicode.com/todos').then(response => response.json()).then(data => {

   	let titleValue =  data.map((elem) => {

   		 return (elem.title)

      })

   	let statusValue =  data.map((elem) => {

   		 return (elem.completed)

      })

   		console.log(`The item \"${titleValue[0]}\" on the list has a status of ${statusValue[0]}`);
});
   



// 5



fetch('https://jsonplaceholder.typicode.com/todos', {
  method: 'POST',
  body: JSON.stringify({
  	"userId": 1,
    title: 'Zuitt Bootcamp',
    completed: true
  }),
  headers: {
    'Content-type': 'application/json; charset=UTF-8',
  },
})
  .then((response) => response.json())
  .then((json) => console.log(json));




// 6



fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json',
	},

	body: JSON.stringify({
		"dateCompleted": "Pending",
		"description": "To update my to do list witha different data structure",
		"id": 1,
		"status": "Pending",
		"title": "Updated to do list item",
		"userId": 1
	})

})
.then((response) => response.json())
.then((json) => console.log(json));




// 7



fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json',
	},

	body: JSON.stringify({
		"Title": "Hello Batch 24",
		"Description": "Update a to do list item by changing the data structure to contain",
		"Status": "Complete",
		"Date Completed": "07/09/21",
		"User ID": 1
	})

})
.then((response) => response.json())
.then((json) => console.log(json));




// 7



fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json',
	},

	body: JSON.stringify({
		"dateCompleted": "Pending",
		"description": "To update my to do list witha different data structure",
		"id": 1,
		"status": "Pending",
		"title": "Updated to do list item",
		"userId": 1
	})

})
.then((response) => response.json())
.then((json) => console.log(json));




// 8




fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE',

 })

